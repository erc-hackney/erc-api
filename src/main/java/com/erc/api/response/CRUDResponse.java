package com.erc.api.response;

import java.sql.Timestamp;

public class CRUDResponse{
    private Object data;
    private int status;
    private String path;
    private String message;
    private Timestamp timestamp;

    public enum Message{
		CREATED("Successfully created data"), 
		UPDATED("Successfully updated data"), 
		DELETED("Successfully deleted data"), 
        VIEWED("Successfully retrieved data"), 
        NOT_FOUND("Resource not found"),
        DUPLICATE("Request rejected as duplicate resource found"),
		ERROR("Something went wrong. No changes have been made");

		private final String message;
		
		Message(final String message){
			this.message = message;
		}

		@Override
		public String toString(){
			return this.message;
		}
    }
    
    public CRUDResponse(){

    }

    public CRUDResponse(Object data, int status, String location, Message message){
        setdata(data);
        setStatus(status);
        setLocation(location);
        setMessage(message);
        setTimestamp(new Timestamp(System.currentTimeMillis()));
    }

    public CRUDResponse(Object data, int status, String location, String message){
        setdata(data);
        setStatus(status);
        setLocation(location);
        this.message = message;
        setTimestamp(new Timestamp(System.currentTimeMillis()));
    }

    /**
	 * @return the timestamp
	 */
	public Timestamp getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return path;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.path = location;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setdata(Object data) {
		this.data = data;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(Message message) {
		this.message = message.toString();
	}
}