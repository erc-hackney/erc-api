package com.erc.api.sermon;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum SessionEnum {
  SUN_MRN("Sunday Morning"),
  SUN_EVE("Sunday Evening"),
  TUE_BIB("Tuesday Bible Study"),
  FRI_BIB("Friday Bible Study"),
  WKD_CNF("Weekend Conference"),
  SPE_SER("Special Service"),
  UNKNOWN("Unknown");

  private final String session;

  SessionEnum(String session) {
    this.session = session;
  }

  public static SessionEnum fromSession(String sessionValue) {
    switch (sessionValue) {
      case "Sunday Morning":
        return SessionEnum.SUN_MRN;
      case "Sunday Evening":
        return SessionEnum.SUN_EVE;
      case "Tuesday Bible Study":
        return SessionEnum.TUE_BIB;
      case "Friday Bible Study":
        return SessionEnum.FRI_BIB;
      case "Weekend Conference":
        return SessionEnum.WKD_CNF;
      case "Special Service":
        return SessionEnum.SPE_SER;
      default:
        return SessionEnum.UNKNOWN;
    }
  }

  public static boolean isSessionEnumValid(String session) {
    return !SessionEnum.fromSession(session).equals(SessionEnum.UNKNOWN);
  }

  public static List<String> validSessionValues() {
    List<String> sessionValues = Stream.of(SessionEnum.values())
            .map(SessionEnum::getSession)
            .collect(Collectors.toList());
    sessionValues.removeIf(x -> x.equals(SessionEnum.UNKNOWN.getSession()));
    return sessionValues;
  }

  public String getSession() {
    return session;
  }
}
