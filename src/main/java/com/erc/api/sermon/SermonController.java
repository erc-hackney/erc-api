package com.erc.api.sermon;

import com.erc.api.response.CRUDResponse;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.Map;

@CrossOrigin
@RestController
public class SermonController {
    private final SermonService sermonService;
    private final static Logger LOGGER = Logger.getLogger(SermonController.class.getName());

    @Autowired
    public SermonController(SermonService sermonService) {
        this.sermonService = sermonService;
    }

    @CrossOrigin
    @GetMapping("/sermon")
    @SuppressWarnings("unused")
    public CRUDResponse getSermons(@RequestHeader Map<String, String> requestHeader,
                                   @RequestParam(value = "orderBy", required = false) String orderBy,
                                   @RequestParam(value = "speaker", required = false) String speaker,
                                   @RequestParam(value = "session", required = false) String session,
                                   @RequestParam(value = "page", defaultValue = "1") Integer page,
                                   @RequestParam(value = "limit", required = false) Integer limit,
                                   @RequestParam(value = "tags", required = false) String[] tags,
                                   @RequestParam(value = "sortAscending", defaultValue = "true") boolean sortAscending) {
        LOGGER.debug(String.format("Request headers: " +
                        "OrderBy='%s', speaker='%s', session='%s', page='%d', limit='%d', sortAscending='%s', tags=%s",
                orderBy, speaker, session, page, limit, sortAscending, tags == null ? "[]" : Arrays.asList(tags)));
        return sermonService.getSermons(
                orderBy,
                speaker,
                session,
                page,
                limit,
                tags,
                sortAscending,
                requestHeader.get("host"));
    }

    @CrossOrigin
    @GetMapping("/sermon/{id}")
    @SuppressWarnings("unused")
    public CRUDResponse getSermon(@PathVariable int id,
                                  @RequestHeader Map<String, String> requestHeader) {
        return sermonService.getSermon(id, requestHeader.get("host"));
    }

    @CrossOrigin
    @GetMapping("/sermon/count")
    @SuppressWarnings("unused")
    public CRUDResponse getSermonCount(@RequestHeader Map<String, String> requestHeader) {
        return sermonService.getSermonCount(requestHeader.get("host"));
    }

    @CrossOrigin
    @GetMapping("/sermon/session")
    @SuppressWarnings("unused")
    public CRUDResponse getSermonSessions(@RequestHeader Map<String, String> requestHeader) {
        return sermonService.getSermonSessions(requestHeader.get("host"));
    }

    /**
     * We will enforce that for uploading sermons, we will upload the file first and on
     * success return the location to the resource to add the accompanying data
     **/
    @CrossOrigin
    @PostMapping(value = "/sermon", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @SuppressWarnings("unused")
    public CRUDResponse uploadSermon(@RequestParam("sermon") MultipartFile sermon,
                                     @RequestHeader Map<String, String> requestHeader) {
        return sermonService.uploadSermon(sermon, requestHeader.get("host"));
    }

    @CrossOrigin
    @PutMapping("/sermon/{id}")
    public CRUDResponse updateSermon(@PathVariable int id,
                                     @RequestBody SermonData sermonData,
                                     @RequestHeader Map<String, String> requestHeader) {
        LOGGER.debug(String.format("Sermon data: %s", sermonData.toString()));
        return sermonService.updateSermon(id, sermonData, requestHeader.get("host"));
    }

    @CrossOrigin
    @PostMapping("/sermon")
    public CRUDResponse insertSermon(@RequestBody SermonData sermonData,
                                     @RequestHeader Map<String, String> requestHeader) {
        LOGGER.debug(String.format("Sermon data: %s", sermonData.toString()));
        return sermonService.insertSermon(sermonData, requestHeader.get("host"));
    }

    @CrossOrigin
    @DeleteMapping("/sermon/{id}")
    @SuppressWarnings("unused")
    public CRUDResponse deleteSermon(@PathVariable int id,
                                     @RequestHeader Map<String, String> requestHeader) {
        return sermonService.deleteSermon(id, requestHeader.get("host"));
    }
}