package com.erc.api.sermon;

import javax.persistence.*;


@Table(name = "sermon_session", uniqueConstraints = @UniqueConstraint(columnNames = {"id", "sessionEnum" }))
@Entity
public class SermonSession{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false, unique = true)
    @SuppressWarnings("unused")
    private int id;
    @Enumerated(EnumType.STRING)
    @Column(unique = true)
    private SessionEnum sessionEnum;

    @SuppressWarnings("unused")
    public SermonSession(String session){
        setSessionEnum(session);
    }

    @SuppressWarnings("unused")
    public SermonSession(){}

    public String toString(){
        return String.format("{ id: %d, session_enum: %s }", getId(), getSessionEnum());
    }

    public String getSessionEnum() {
        return this.sessionEnum.getSession();
    }

    public void setSessionEnum(String session) {
        this.sessionEnum = SessionEnum.fromSession(session);
    }

    private int getId() {
        return id;
	}
}