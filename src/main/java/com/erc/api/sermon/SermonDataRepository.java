package com.erc.api.sermon;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface SermonDataRepository extends CrudRepository<SermonData, Integer>{
    Optional<SermonData> findByFileName(String fileName);
}