package com.erc.api.sermon;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Table(name = "sermon_data")
@Entity
public class SermonData{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
   	@Column(updatable = false, nullable = false, unique = true)
	@SuppressWarnings("unused")
	private int id;
	@Column(unique = true)
	private String fileName;
	private String name;
	private String speaker;
	private int duration;
	@ManyToOne()
	@JoinColumn(name = "sermon_session_id", referencedColumnName = "id")
	private SermonSession sermonSession;
	private LocalDate date;
	private LocalDate uploadDate;
	private String description;
	@ElementCollection
	@CollectionTable(
		name = "SERMON_DATA_TAGS",
		joinColumns = @JoinColumn(name = "id", referencedColumnName = "id"))
	@Column(name = "tag")
	private List<String> tags;

	SermonData(String fileName, int duration){
		setFileName(fileName);
		setName("");
		setSpeaker("");
		setDuration(duration);
		setSermonSession(null);
		setDate(LocalDate.now());
		setUploadDate(LocalDate.now());
		setDescription("");
		setTags(null);
	}

	@SuppressWarnings("unused")
	public SermonData(){
		setFileName("");
		setName("");
		setSpeaker("");
		setDuration(0);
		setSermonSession(null);
		setDate(null);
		setUploadDate(LocalDate.now());
		setDescription("");
		setTags(null);
	}

	public String toString(){
		return String.format("{ " +
				"id: %d, " +
				"file_name: %s, " +
				"name: %s, " +
				"speaker: %s, " +
				"duration: %d, " +
				"sermon_session: %s, " +
				"date: %s, " +
				"upload_date: %s, " +
				"description: %s, " +
				"tags: %s}",
				getId(),
				getFileName(),
				getName(),
				getSpeaker(),
				getDuration(),
				getSermonSession(),
				getDate(),
				getUploadDate(),
				getDescription(),
				getTags() == null ? "[]" : Collections.singletonList(getTags()));
	}

	public SermonSession getSermonSession() {
		return this.sermonSession;
	}

	public void setSermonSession(SermonSession sermonSession) {
		this.sermonSession = sermonSession;
	}

	public String getSpeaker() {
		return speaker;
	}

	public void setSpeaker(String speaker) {
		this.speaker = speaker;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getId() {
		return id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
    }

	public LocalDate getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(LocalDate uploadDate) {
		this.uploadDate = uploadDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}