package com.erc.api.sermon;

import com.erc.api.aws.AwsService;
import com.erc.api.properties.ApplicationConfiguration;
import com.erc.api.response.CRUDResponse;
import com.erc.api.response.CRUDResponse.Message;
import com.erc.api.util.UtilService;
import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.commons.collections.comparators.NullComparator;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.SupportedFileFormat;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
class SermonService {
    private final static Logger LOGGER = Logger.getLogger(SermonService.class.getName());
    @Autowired
    private SermonDataRepository sermonDataRepository;
    @Autowired
    private SermonSessionRepository sermonSessionRepository;
    @Autowired
    private ApplicationConfiguration applicationConfiguration;
    @Autowired
    private AwsService awsService;
    @Autowired
    private UtilService utilService;
    @Value("${spring.profiles.active}")
    private String activeProfile;

    private CRUDResponse validateFields(String orderBy,
                                        String session,
                                        Integer page,
                                        Integer limit,
                                        String host) {
        if (session != null) {
            if (!SessionEnum.isSessionEnumValid(session)) {
                return new CRUDResponse(
                        null,
                        HttpStatus.UNPROCESSABLE_ENTITY.value(),
                        String.format("%s%s", host, applicationConfiguration.getRestSermonPath()),
                        String.format("Request parameter session='%s' is not in the list of acceptable session " +
                                      "fields: %s", session, SessionEnum.validSessionValues()));
            }
        }

        if (page != null && page < 1) {
            return new CRUDResponse(
                    null,
                    HttpStatus.UNPROCESSABLE_ENTITY.value(),
                    String.format("%s%s", host, applicationConfiguration.getRestSermonPath()),
                    String.format("Request parameter page='%d' is invalid and must be greater than zero.", page));
        }

        if (limit != null && limit < 1) {
            return new CRUDResponse(
                    null,
                    HttpStatus.UNPROCESSABLE_ENTITY.value(),
                    String.format("%s%s", host, applicationConfiguration.getRestSermonPath()),
                    String.format("Request parameter limit='%d' is invalid and must be greater than zero.", limit));
        }

        if (orderBy != null) {
            long validOrderBy = applicationConfiguration.getSupportedOrderFields().stream()
                                .filter(orderBy::equals).count();
            if (validOrderBy == 0) {
                return new CRUDResponse(
                        null,
                        HttpStatus.UNPROCESSABLE_ENTITY.value(),
                        String.format("%s%s", host, applicationConfiguration.getRestSermonPath()),
                        String.format("Request parameter orderBy='%s' is not in the list of acceptable orderBy " +
                                "fields: %s", orderBy, applicationConfiguration.getSupportedOrderFields().toString()));
            }
        }

        return null;
    }

    @SuppressWarnings("unchecked, sort")
    CRUDResponse getSermons(String orderBy,
                            String speaker,
                            String session,
                            Integer page,
                            Integer limit,
                            String[] tags,
                            boolean sortAscending,
                            String host) {
        CRUDResponse errors = validateFields(orderBy, session, page, limit, host);
        if (errors != null) {
            return errors;
        }
        List<SermonData> sermonDataList = new ArrayList<>();
        sermonDataRepository.findAll().forEach(sermonDataList::add);

        // If there is a tags value, then only get results that have tags that match at least one of the tags
        if (!sermonDataList.isEmpty() && tags != null) {
            sermonDataList = sermonDataList.stream()
                    .filter(x -> x.getTags().stream()
                            .anyMatch(new HashSet<>(Arrays.asList(tags))::contains))
                    .collect(Collectors.toList());
        }

        // If there is a valid speaker value, then only get results that have the same speaker value
        if (!sermonDataList.isEmpty() && speaker != null) {
            sermonDataList = sermonDataList.stream()
                    .filter(x -> x.getSpeaker().equals(speaker))
                    .collect(Collectors.toList());
        }

        // If there is a valid session value, then only get results that have the same session value
        if (!sermonDataList.isEmpty() && session != null) {
            sermonDataList = sermonDataList.stream()
                    .filter(x -> x.getSermonSession().getSessionEnum().equals(session))
                    .collect(Collectors.toList());
        }

        // If there is a valid orderBy field, then return results in ordered by the appropriate property
        // If sortAscending is false i.e. descending order is chosen, then the order will be flipped
        if(!sermonDataList.isEmpty() && orderBy != null){
            ComparatorChain comparatorChain = new ComparatorChain();
            Comparator nullComparator = new BeanComparator(orderBy, new NullComparator(true) );
            comparatorChain.addComparator(nullComparator);
            if(!sortAscending) {
                Collections.sort(sermonDataList, comparatorChain.reversed());
            } else {
                Collections.sort(sermonDataList, comparatorChain);
            }
        }

        // Enforces the limit if it is set and greater than the number of entries
        if (!sermonDataList.isEmpty() && limit != null && (long) sermonDataList.size() > limit) {
            if (page != null && page > 1) {
                sermonDataList = sermonDataList.stream()
                        .skip(limit * (page - 1))
                        .limit(limit)
                        .collect(Collectors.toList());
            } else {
                sermonDataList = sermonDataList.stream()
                        .limit(limit)
                        .collect(Collectors.toList());
            }
        }

        return new CRUDResponse(
                sermonDataList,
                HttpStatus.OK.value(),
                String.format("%s%s", host, applicationConfiguration.getRestSermonPath()),
                Message.VIEWED
        );
    }

    CRUDResponse getSermon(int id, String host) {
        if (sermonDataRepository.findById(id).isPresent()) {
            SermonData sermonData = sermonDataRepository.findById(id).get();
            LOGGER.debug(sermonData);
            return new CRUDResponse(
                    sermonData,
                    HttpStatus.OK.value(),
                    String.format("%s%s/%d",
                    host,
                    applicationConfiguration.getRestSermonPath(), id),
                    Message.VIEWED
            );
        } else
            return new CRUDResponse(null, HttpStatus.NOT_FOUND.value(), host + applicationConfiguration.getRestSermonPath() + "/" + id, Message.NOT_FOUND);
    }

    CRUDResponse insertSermon(SermonData sermonData, String host) {
        SermonData newSermon;
        try {
            newSermon = new SermonData(sermonData.getFileName(), 0);
        } catch (Exception e) {
            return new CRUDResponse(
                    e.getMessage(),
                    HttpStatus.BAD_REQUEST.value(),
                    String.format("%s%s", host, applicationConfiguration.getRestSermonPath()),
                    Message.ERROR);
        }

        // Check that the sermon session field and the nested Session Enum is not empty before proceeding
        // Check that we have a valid session enum value
        if (sermonData.getSermonSession() != null && sermonData.getSermonSession().getSessionEnum() != null) {
            if (SessionEnum.isSessionEnumValid(sermonData.getSermonSession().getSessionEnum())) {
                SessionEnum sessionEnum = SessionEnum.fromSession(sermonData.getSermonSession().getSessionEnum());
                LOGGER.debug(String.format("Session enum: %s", sessionEnum));
                SermonSession childSermonSession = sermonSessionRepository.findBySessionEnum(sessionEnum);
                LOGGER.debug(String.format("Sermon session: %s", childSermonSession));
                newSermon.setSermonSession(childSermonSession);
            } else {
                return new CRUDResponse(
                        null,
                        HttpStatus.UNPROCESSABLE_ENTITY.value(),
                        String.format("%s%s", host, applicationConfiguration.getRestSermonPath()),
                        String.format("sermonSession.sessionEnum is not in the list of acceptable session enum " +
                                "fields: %s", SessionEnum.validSessionValues())
                );
            }
        }

        if (sermonData.getName() != null) {
            newSermon.setName(sermonData.getName());
        }
        if (sermonData.getSpeaker() != null) {
            newSermon.setSpeaker(sermonData.getSpeaker());
        }
        if (sermonData.getTags() != null) {
            newSermon.setTags(sermonData.getTags());
        }
        if (sermonData.getDescription() != null) {
            newSermon.setDescription(sermonData.getDescription());
        }
        if (sermonData.getDate() != null) {
            newSermon.setDate(sermonData.getDate());
        }
        sermonDataRepository.save(newSermon);
        LOGGER.debug(String.format("Sermon data: %s", newSermon.toString()));
        return new CRUDResponse(
                newSermon,
                HttpStatus.CREATED.value(),
                String.format("%s%s/%d", host, applicationConfiguration.getRestSermonPath(), newSermon.getId()),
                Message.CREATED);
    }

    CRUDResponse uploadSermon(MultipartFile sermon, String host) {
        SermonData createdSermonData;
        if (sermonDataRepository.findByFileName(sermon.getOriginalFilename()).isPresent()) {
            int duplicateId = sermonDataRepository.findByFileName(sermon.getOriginalFilename()).get().getId();
            return new CRUDResponse(
                    null,
                    HttpStatus.FORBIDDEN.value(),
                    String.format("%s%s/%d", host, applicationConfiguration.getRestSermonPath(), duplicateId),
                    Message.DUPLICATE);
        } else {
            try {
                String fileName = sermon.getOriginalFilename();
                String fileExtension = null;

                if (fileName != null) {
                    fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
                }

                if (fileExtension != null && !fileExtension.equals(SupportedFileFormat.MP3.getFilesuffix())) {
                    return new CRUDResponse(
                            null,
                            HttpStatus.UNSUPPORTED_MEDIA_TYPE.value(),
                            String.format("%s%s", host, applicationConfiguration.getRestSermonPath()),
                            String.format("Content type of '%s' not accepted. Supported formats are: [MP3]",
                            sermon.getContentType())
                    );
                }

                SermonData uploadedSermon;
                if (activeProfile.equals("dev") || activeProfile.equals("uat")) {
                    LOGGER.debug("Starting local upload");
                    uploadedSermon = localUpload(sermon, fileName);
                } else {
                    LOGGER.debug("Starting AWS upload");
                    uploadedSermon = awsUpload(sermon, fileName);
                }

                if (uploadedSermon != null) {
                    sermonDataRepository.save(uploadedSermon);
                    LOGGER.debug("Retrieving id from database to verify new entry");
                    createdSermonData = sermonDataRepository.findByFileName(uploadedSermon.getFileName()).orElse(null);
                    LOGGER.debug(createdSermonData);
                } else {
                    createdSermonData = null;
                }
            } catch (Exception e) {
                return new CRUDResponse(
                        e.getMessage(),
                        HttpStatus.BAD_REQUEST.value(),
                        String.format("%s%s", host, applicationConfiguration.getRestSermonPath()),
                        Message.ERROR);
            }

            if(createdSermonData != null) {
                return new CRUDResponse(
                        createdSermonData,
                        HttpStatus.CREATED.value(),
                        String.format("%s%s/%d", host, applicationConfiguration.getRestSermonPath(), createdSermonData.getId()),
                        Message.CREATED);
            } else {
                return new CRUDResponse(
                        null,
                        HttpStatus.BAD_REQUEST.value(),
                        String.format("%s%s", host, applicationConfiguration.getRestSermonPath()),
                        Message.ERROR);
            }
        }
    }

    CRUDResponse updateSermon(int id, SermonData sermonData, String host) {
        if (sermonDataRepository.findById(id).isPresent()) {
            SermonData updateSermon = sermonDataRepository.findById(id).get();

            // Check that the sermon session field and the nested Session Enum is not empty before proceeding
            // Check that we have a valid session enum value
            if (sermonData.getSermonSession() != null && sermonData.getSermonSession().getSessionEnum() != null) {
                if (SessionEnum.isSessionEnumValid(sermonData.getSermonSession().getSessionEnum())) {
                    SessionEnum sessionEnum = SessionEnum.fromSession(sermonData.getSermonSession().getSessionEnum());
                    LOGGER.debug(String.format("Session enum: %s", sessionEnum));
                    SermonSession childSermonSession = sermonSessionRepository.findBySessionEnum(sessionEnum);
                    LOGGER.debug(String.format("Sermon session: %s", childSermonSession));
                    updateSermon.setSermonSession(childSermonSession);
                } else {
                    return new CRUDResponse(
                            null,
                            HttpStatus.UNPROCESSABLE_ENTITY.value(),
                            String.format("%s%s", host, applicationConfiguration.getRestSermonPath()),
                            String.format("sermonSession.sessionEnum is not in the list of acceptable session enum " +
                                          "fields: %s", SessionEnum.validSessionValues())
                    );
                }
            }

            if (sermonData.getName() != null) {
                updateSermon.setName(sermonData.getName());
            }
            if (sermonData.getSpeaker() != null) {
                updateSermon.setSpeaker(sermonData.getSpeaker());
            }
            if (sermonData.getTags() != null) {
                updateSermon.setTags(sermonData.getTags());
            }
            if (sermonData.getDescription() != null) {
                updateSermon.setDescription(sermonData.getDescription());
            }
            if (sermonData.getDate() != null) {
                updateSermon.setDate(sermonData.getDate());
            }

            sermonDataRepository.save(updateSermon);

            return new CRUDResponse(
                    updateSermon,
                    HttpStatus.OK.value(),
                    String.format("%s%s/%d", host, applicationConfiguration.getRestSermonPath(), id),
                    Message.UPDATED
            );
        } else
            return new CRUDResponse(
                    null,
                    HttpStatus.NOT_FOUND.value(),
                    String.format("%s%s/%d", host, applicationConfiguration.getRestSermonPath(), id),
                    Message.NOT_FOUND
            );
    }

    CRUDResponse deleteSermon(int id, String host) {
        if (sermonDataRepository.findById(id).isPresent()) {
            SermonData deleteSermon = sermonDataRepository.findById(id).get();
            LOGGER.debug(String.format("Deleting %s", deleteSermon.getFileName()));
            awsService.deleteFileFromS3Bucket(deleteSermon.getFileName());
            sermonDataRepository.delete(deleteSermon);
            return new CRUDResponse(
                    null,
                    HttpStatus.OK.value(),
                    String.format("%s%s/%d",
                        host,
                        applicationConfiguration.getRestSermonPath(),
                        id
                    ),
                    Message.DELETED
            );
        } else
            return new CRUDResponse(
                    null,
                    HttpStatus.NOT_FOUND.value(),
                    String.format("%s%s/%d", host, applicationConfiguration.getRestSermonPath(), id),
                    Message.NOT_FOUND
            );
    }

    CRUDResponse getSermonCount(String host) {
        return new CRUDResponse(
                sermonDataRepository.count(),
                HttpStatus.OK.value(),
                String.format("%s%s/count", host, applicationConfiguration.getRestSermonPath()),
                Message.VIEWED
        );
    }

    CRUDResponse getSermonSessions(String host) {
        List<SermonSession> sermonSessions = new ArrayList<>();
        sermonSessionRepository.findAll().forEach(sermonSessions::add);
        return new CRUDResponse(
                sermonSessions,
                HttpStatus.OK.value(),
                String.format("%s%s/session", host, applicationConfiguration.getRestSermonPath()),
                Message.VIEWED
        );
    }

    private SermonData localUpload(MultipartFile sermon, String fileName) {
        try {
            // Retrieve sermon file path
            File current = ResourceUtils.getFile("classpath:");
            String sermonPath = String.format("%s/%s", current.getPath(), fileName);
            LOGGER.debug(String.format("File path: %s", sermonPath));
            File sermonFile = new File(sermonPath);

            // Create new sermon file in classpath
            boolean newFileCreated = sermonFile.createNewFile();
            if (newFileCreated) {
                FileOutputStream fOut = new FileOutputStream(sermonFile);
                fOut.write(sermon.getBytes());
                fOut.close();
            }

            int duration = postUploadProcessing(sermonFile, fileName);
            return new SermonData(fileName, duration);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private SermonData awsUpload(MultipartFile sermon, String filename) {
        try {
            String awsFilename = awsService.uploadFile(sermon, filename);
            File sermonFile = utilService.convertMultiPartToFile(sermon);
            int duration = postUploadProcessing(sermonFile, awsFilename);
            return new SermonData(awsFilename, duration);
        }
        catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private int postUploadProcessing(File sermonFile, String fileName) {
        try {
            int duration;
            LOGGER.debug("Extracting duration from audio header");
            MP3File audioFile = (MP3File) AudioFileIO.read(sermonFile);
            int seconds = audioFile.getAudioHeader().getTrackLength();
            duration = Integer.parseInt(String.format("%02d", TimeUnit.SECONDS.toMinutes(seconds)));
            LOGGER.debug("Creating new sermon data instance to hold metadata");
            LOGGER.debug(String.format("File name: %s", fileName));
            LOGGER.debug(String.format("Duration: %d", duration));
            return duration;
        }
        catch(Exception e) {
            return 0;
        }
    }
}