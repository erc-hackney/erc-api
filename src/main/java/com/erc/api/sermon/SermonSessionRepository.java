package com.erc.api.sermon;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SermonSessionRepository extends CrudRepository<SermonSession, Integer>{
    SermonSession findBySessionEnum(SessionEnum sessionEnum);
}