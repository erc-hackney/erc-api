package com.erc.api.home;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class HomeController{
    @GetMapping("/")
    public String getHome(){
        return
            "ERC Hackney API";
    }
}