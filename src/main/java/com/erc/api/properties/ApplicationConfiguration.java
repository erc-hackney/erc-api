package com.erc.api.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@ConfigurationProperties("application")
public class ApplicationConfiguration{
    private String sermon_path;
    private static final String rest_sermon_path = "/sermon";
    private static final List<String> supportedOrderFields = Arrays.asList("date", "duration", "name", "speaker");

    public String getSermonPath(){
        return sermon_path;
    }

    public void setSermonPath(String sermon_path){
        this.sermon_path = sermon_path;
    }

    public String getRestSermonPath(){
        return rest_sermon_path;
    }

    public List<String> getSupportedOrderFields(){
        return supportedOrderFields;
    }
}