package com.erc.api.aws;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.erc.api.util.UtilService;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

@Service
public class AwsService {
    @Autowired
    private UtilService utilService;
    @Value("${application.aws_access_key}")
    private String awsAccessKey;
    @Value("${application.aws_secret_key}")
    private String awsSecretKey;
    @Value("${application.aws_bucket}")
    private String awsBucket;
    @Value("${application.aws_endpoint_url}")
    private String awsEndpointUrl;
    private AmazonS3 s3Client;
    private final static Logger LOGGER = Logger.getLogger(AwsService.class.getName());

    @PostConstruct
    @SuppressWarnings("unused")
    private void initializeAmazon() {
        BasicAWSCredentials creds = new BasicAWSCredentials(this.awsAccessKey, this.awsSecretKey);
        this.s3Client = AmazonS3ClientBuilder.standard()
                .withRegion("eu-west-2")
                .withCredentials(new AWSStaticCredentialsProvider(creds)).build();
    }

    private void uploadFileTos3Bucket(String fileName, File file) {
        s3Client.putObject(new PutObjectRequest(awsBucket, fileName, file)
                .withCannedAcl(CannedAccessControlList.PublicRead));
    }

    public void deleteFileFromS3Bucket(String fileUrl) {
        String fileName;
        try {
            fileName = URLDecoder.decode(fileUrl.substring(fileUrl.lastIndexOf("/") + 1), "UTF-8");
            LOGGER.debug(String.format("File name: %s", fileName));
            s3Client.deleteObject(new DeleteObjectRequest(awsBucket, fileName));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public String uploadFile(MultipartFile multipartFile, String filename) {
        String fileUrl = null;
        try {
            LOGGER.debug(String.format("Endpoint URL: %s", awsEndpointUrl));
            LOGGER.debug(String.format("Bucket: %s", awsBucket));
            LOGGER.debug(String.format("Filename: %s", filename));
            File file = utilService.convertMultiPartToFile(multipartFile);
            fileUrl = String.format("%s/%s/%s", awsEndpointUrl, awsBucket, filename);
            uploadFileTos3Bucket(filename, file);
            boolean success = file.delete();
            if (success) {
                LOGGER.debug("Temporary file has been deleted");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileUrl;
    }
}
