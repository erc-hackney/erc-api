#Hackney ERC API

The Hackney ERC API allows communication between the front-end (the ERC website/mobile website) and the back-end (the database). Functions such as searching, listing, creating, updating and deleting data would require calls to the Hackney ERC API via the documented REST API calls.

The current supported APIs are:

1. Sermon

    * [POST /sermon](#markdown-header-post-sermon)
    * [PUT /sermon/{id}](#markdown-header-put-sermonid)
    * [DELETE /sermon/{id}](#markdown-header-delete-sermonid)
    * [GET /sermon/{id}](#markdown-header-get-sermonid)
    * [GET /sermon](#markdown-header-get-sermon)

## Sermon API

The sermon API allows you to upload physical sermons in `MP3` format to the server. Currently, any other format than `MP3` will be rejected (although eventually we would like to support `MP4` if we decide to video messages). Another consideration is that each request to upload a sermon is limited to `60MB` by default. This can be changed by updating the value in `src/main/resources/application.properties`.

### POST /sermon
This request allows you to upload a sermon file to the server. This request will only upload the sermon file without the metadata. This design is so that if the request were to fail by any chance, the metadata would not be lost in the request. This design choice means that adding a new sermon will require to seperate requests; one to send the MP3 and on to upload the metadata (see `PUT /sermon`).

An exmple request to add a new sermon would look like the following:
```bash
curl --request POST \
	--header 'content-type: multipart/form-data' \
    --form sermon={PATH_TO_MP3_FILE}.mp3 \
    --url http://{SERVER_HOST:SERVER_PORT}/sermon
```
On a successful post, the sermon is uploaded and a placeholder for the Sermon data is created. The `id` for this record is returned to be used in the `PUT` request to add the metadata. The placeholder also stores the length of the sermon as well as the name of file uploaded to help identify the record to prevent duplicate uploads. 

An example payload would look like the following:
```json
{
	"data": {
		"id": 7,
		"fileName": "Lords Day Evening Being Filled with the Fullness of God .mp3",
		"name": "",
		"speaker": "",
		"duration": 50,
		"date": null,
		"uploadDate": "2018-07-03",
		"description": "",
		"tags": [
			""
		]
	},
	"status": 201,
	"message": "Successfully created data",
	"timestamp": "2018-07-03T11:18:52.843+0000",
	"location": "localhost:8080/sermon/7"
}
```

### PUT /sermon/{id}
This request allows you to update the **metadata** linked to a sermon. This call can only be made once a sermon has been uploaded to the server successfully (using `POST /sermon`) and the `id` is returned to the user. The filename, id, upload date and duration are fields that can **NOT** be updated as they are tied to when the physical sermon file was uploaded and are automatically generated fields. An example request would look as follow:

```bash
curl --request PUT \
	--header 'content-type: application/json' \
	--data '{
		"name": "Being Filled with the Fullness of God",
	    "speaker": "Elvin Mensah",
	    "date": "2018-02-18",
	    "description": "Ephesians 3:14-19",
	    "tags": [ "spiritual", "growth", "maturity"]
	}' \
	--url http://localhost:8080/sermon/7
```

* The `date` field refers to the date the sermon was given.
* The `speaker` field is the preacher who gives the sermon
* The `name` field is the displayed name for the sermon to be used in the front-end
* The `description` field can be any text that desribes the sermon e.g. scriptures, key points
* The `tags` array is a list of tags that the user can use to filter through all the sermons implementing basic search

On a successful `PUT`, one can expect the following results:

```json
{
	"data": {
		"id": 7,
		"fileName": "Lords Day Evening Being Filled with the Fullness of God.mp3",
		"name": "Being Filled with the Fullness of God",
		"speaker": "Elvin Mensah",
		"duration": 50,
		"date": "2018-02-18",
		"uploadDate": "2018-07-03",
		"description": "Ephesians 3:14-19",
		"tags": [
			"spiritual",
			"growth",
			"maturity"
		]
	},
	"status": 200,
	"message": "Successfully updated data",
	"timestamp": "2018-07-03T11:41:39.186+0000",
	"location": "localhost:8080/sermon/7"
}
```

### DELETE /sermon/{id}

This API call allows you to delete a sermon from the server. This will delete **both** the sermon's metadata and the `MP3` file itself. On a successful delete, the data associated with sermon, a status 200, a message indicated the delete was successful, the timestamp and a location to where the request was made will be returned in the response.

### GET /sermon/{id}

This API call allows you to retrieve a single sermon's **metadata only**. This will not retrieve the `MP3` file itself. On a successful get, the data associated with sermon, a status 200, a message indicated the get was successful, the timestamp and a location to where the request was made will be returned in the response. The __filename__ within the data can be used to retrieve the sermon's `MP3` file from the Cloud storage solution.

### GET /sermon

This API call allows you to retrieve multiple sermon's **metadata**. This is useful in the use case of searching for a sermon and supports the following functions:

* Get all sermons:

```bash
	curl --request GET \
  		--header 'content-type: application/json' \
  		--url 'http://localhost:8080/sermon'
```

* Filtering via the supported fields: _speaker_, _tags_ e.g. To search for sermons by the speaker "Kehinde Omotayo":

```bash
	curl --request GET \
  		--header 'content-type: application/json' \
  		--url 'http://localhost:8080/sermon?speaker=Kehinde%20Omotayo'
```

* Ordering via the supported fields: _date_, _duration_, _name_, _speaker_ e.g. To order results by the _name_ of each sermon in ascending order:

```bash
	curl --request GET \
		--header 'content-type: application/json' \
  		--url 'http://localhost:8080/sermon?orderBy=name'
```
		
* Sorting in ascending order by default e.g. Set `sortAscending=false` to return the results in descending order:

```bash
	curl --request GET \
  		--url 'http://localhost:8080/sermon?sortAscending=false'
  		--header 'content-type: application/json'
```
			
* Paginated results (set `page=${pageNumber}` and `limit=${maxResultsOnPage}`) e.g. To limit results to 5 sermons a page and get the 2nd page:

```bash
	curl --request GET \
		--header 'content-type: application/json' \
  		--url 'http://localhost:8080/sermon?limit=5&page=2'
```
 
Of course, each of these different options can be used in combination e.g. To search for the first set of 10 sermons where the sermon is tagged with _law_ and the speaker is "Kehinde Omotayo" ordered by _name_ in descending order, you would end up with the following request:

```bash
curl --request GET \
	--header 'content-type: application/json' \
  	--url 'http://localhost:8080/sermon?limit=10&orderBy=name&page=1&sortAscending=false&speaker=Kehinde%20Omotayo&tags=law'
```

---

## Address API

Coming soon