#!/bin/bash

Log() {
  echo "$(date '+%Y-%m-%d %H:%M:%S') - $1"
}

start=$(date +"%s")

Log "Starting sermon processing"
export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=
aws s3 ls s3://media.erc-hackney.com/sermons --recursive > ./sermons.txt

if [[ -f `pwd`/sermons.txt ]]; then   
  sed -i '1d' ./sermons.txt
else 
  Log "`pwd`/sermons.txt not found. Stopping process!"
  exit 1
fi

echo "INSERT INTO SERMON_SESSION (session_enum) VALUES 
('SUN_MRN'), 
('SUN_EVE'), 
('TUE_BIB'), 
('FRI_BIB'), 
('WKD_CNF');" > data.sql
echo "" >> data.sql

echo "INSERT INTO SERMON_DATA (file_name, name, speaker, duration, date, upload_date, description, sermon_session_id) VALUES" >> ../src/main/resources/data.sql

while read -r line; do
  sermon_data=`echo $line | cut -d " " -f4`

  file_name="http://media.erc-hackney.com.s3.amazonaws.com/${sermon_data}"
  name=`echo $sermon_data | cut -d "~" -f3 | sed 's/_/ /g'`
  speaker=`echo $sermon_data | cut -d "~" -f4 | sed 's/_/ /g' | sed 's/.mp3//g'` 
  duration=`ffmpeg -i $file_name 2>&1 | grep Duration | cut -d ":" -f3`
  date=`echo $sermon_data | cut -d "~" -f1 | sed 's/_/ /g' | sed 's/sermons\///g'`
  upload_date=`echo $line | cut -d " " -f1`
  session=`echo $line | cut -d "~" -f2`
  description=

  Log "Started processing $name sermon by $speaker preached on $date" 
  Log "Duration set to: $duration"

  if [[ $name == *"Morning"* ]]; then
    sermon_session_id=1
    Log "Session id set to: $sermon_session_id"
  fi

  if [[ $name == *"Evening"* ]]; then
    sermon_session_id=2
    Log "Session id set to: $sermon_session_id"
  fi

  if [[ $session == "Bible_Study_and_Prayer_Meeting" ]]; then
      sermon_session_id=3
      Log "Session id set to: $sermon_session_id"
  fi

  if [[ ( $session == "Our_Beliefs" || $session == "Friday_Bible_Study" || $session == "Young_People" ) ]]; then
      sermon_session_id=4
      Log "Session id set to: $sermon_session_id"
  fi

  if [[ ( $name == *"Voddie"* || $name == "*Beeke*" ) ]]; then
      sermon_session_id=5
      Log "Session id set to: $sermon_session_id"
  fi

  if [[ $sermon_session_id == "" ]]; then
    Log "Session id not found!"
  fi

  sql_file_name=\'$(echo $file_name | sed s/\'/\'\'/g)\'
  sql_name=\'$(echo $name | sed s/\'/\'\'/g)\'
  sql_speaker=\'$(echo $speaker | sed s/\'/\'\'/g)\'
  sql_date=\'$(echo $date | sed s/\'/\'\'/g)\'
  sql_upload_date=\'$(echo $upload_date | sed s/\'/\'\'/g)\'
  sql_description=\'$(echo $description | sed s/\'/\'\'/g)\'
  sql="($sql_file_name, $sql_name, $sql_speaker, $duration, $sql_date, $sql_upload_date, $sql_description, $sermon_session_id),"
  echo $sql >> data.sql

  Log "Sql to add is: $sql"
  Log "Completed processing $name sermon by $speaker preached on $date" 
done < ./sermons.txt

end=$(date +"%s")
runtime=$(($end-$start))

Log "Remember... please edit the file to correct the SQL before pushing!"
Log "Finished processing sermons in $(($runtime/60)) minutes and $(($runtime % 60)) seconds"